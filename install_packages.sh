#!/bin/sh
sudo apt-get install xscrensaver alsa-utils pasystray conky scrot numlockx xinit awesome vim-gtk iceweasel python3-pip libgtk2.0-0 ntfs-3g network-manager-gnome 
# ly requisites
sudo apt-get install build-essential libpam0g-dev libxcb-xkb-dev

# machine specific
sudo apt-get install blueman firmware-atheros firmware-realtek firmware-linux 

#install for docker
sudo apt-get install apt-transport-https ca-certificates gnupg2 software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce

#docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo curl -L https://raw.githubusercontent.com/docker/compose/1.25.0/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose
sudo usermod -aG docker ${USER}

#VUNDLE for vim
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

# install ly

make github -C $PWD/ly/
make -C $PWD/ly/
sudo make install -C $PWD/ly/
sudo systemctl enable ly.service
