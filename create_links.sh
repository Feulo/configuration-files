#!/bin/sh
mkdir -p /home/$USER/.config/awesome
mkdir -p /home/$USER/.ssh/
ln -sf $PWD/awesome/rc.lua /home/$USER/.config/awesome/rc.lua
ln -sf $PWD/.bashrc /home/$USER/.bashrc
ln -sf $PWD/.bash_aliases /home/$USER/.bash_aliases
ln -sf $PWD/.conkyrc /home/$USER/.conkyrc
ln -sf $PWD/.gitignore_global /home/$USER/.gitignore_global
ln -sf $PWD/.profile /home/$USER/.profile
ln -sf $PWD/.xscreensaver /home/$USER/.xscreensaver
ln -sf $PWD/.gitconfig /home/$USER/.gitconfig
ln -sf $PWD/.vimrc /home/$USER/.vimrc
ln -sf $PWD/.Xresources /home/$USER/.Xresources
ln -sf $PWD/.xinitrc /home/$USER/.xinitrc
ln -sf $PWD/.xsession /home/$USER/.xsession
#for file in $PWD/.ssh/*;do
#    ln -sf $file /home/$USER/.ssh/$(basename $file)
#done
#find $PWD/.ssh -type f -exec chmod -R 600 {} \;
