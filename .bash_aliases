alias classroom='python3.7 ~/classroom-cli/classroom-cli.py'
alias moss='~/moss/moss.pl'
alias pytest='python3 -m pytest'
alias display='~/ULXrandR/src/lxrandr'
alias webcam='VBoxManage controlvm win7 webcam attach .1'
alias netbeans='/usr/local/netbeans-11.3/netbeans/bin/netbeans'
alias dcx='docker-compose exec'

